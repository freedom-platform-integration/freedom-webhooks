# Freedom Platform Webhook Integration

[![Deploy](https://get.pulumi.com/new/button.svg)](https://app.pulumi.com/new)

## Overview

This repository contains a reference implementation of the [Freedom Platform](https://www.freedom-platform.com) webhook callback interface for telemetry events, architected as an Azure serverless application, and deployed using Pulumi. If you prefer to deploy to a different cloud or on-prem environment, you can fork this repository and adapt the code in the [functions/](functions/) directory to suit your needs.

## Prerequisites

In order to deploy this integration, you will need

- A linux/WSL shell environment (if you do not have one locally, you can use the [Azure Cloud Shell](https://azure.microsoft.com/en-us/features/cloud-shell/))
- [Node.js and npm](https://nodejs.org/en/download/) (>= v12.18.3 LTS)
- [Microsoft Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli?view=azure-cli-latest)
- [Pulumi CLI](https://www.pulumi.com/docs/get-started/install/)
- A SQL Server query tool/cli

## Preparing For Deployment 

N.B. Please review and update all resource names/credentials below. Values provided are for reference/illustration only and may not match your organization's conventions.

1.  Choose a storage location for your Pulumi state (pick one):
    - \[**Recommended**\] Using Pulumi Cloud (create an account at https://www.pulumi.com/)
        ```console
        foo@bar:~$ pulumi login
        ```

    - Using a new Microsoft Azure Blob Storage container
        ```console
        foo@bar:~$ PULIMI_STATE_RG="pulumi-state-rg"
        foo@bar:~$ PULIMI_STATE_STORAGE="mypulumistate"
        foo@bar:~$ PULIMI_STATE_CONTAINER="pulumi-state"
        foo@bar:~$ az login
        foo@bar:~$ az group create --name $PULIMI_STATE_RG --location EastUS
        foo@bar:~$ az storage account create --name $PULIMI_STATE_STORAGE --resource-group $PULIMI_STATE_RG --location EastUS --sku Standard_ZRS --encryption-services blob
        foo@bar:~$ export AZURE_STORAGE_ACCOUNT=$PULIMI_STATE_STORAGE
        foo@bar:~$ KEYS=$(az storage account keys list --account-name $AZURE_STORAGE_ACCOUNT --resource-group $PULIMI_STATE_RG --output json)
        foo@bar:~$ export AZURE_STORAGE_KEY=$(echo $KEYS | jq -r .[0].value)
        foo@bar:~$ az storage container create --name $PULIMI_STATE_CONTAINER --fail-on-exist 
        foo@bar:~$ pulumi login azblob://$PULIMI_STATE_CONTAINER
        ```
    
    - Using an existing Microsoft Azure Blob Storage container
        ```console
        foo@bar:~$ PULIMI_STATE_CONTAINER="pulumi-state"
        foo@bar:~$ export AZURE_STORAGE_ACCOUNT="mypulumistate"
        foo@bar:~$ az login
        foo@bar:~$ KEYS=$(az storage account keys list --account-name $AZURE_STORAGE_ACCOUNT --resource-group $PULIMI_STATE_RG --output json)
        foo@bar:~$ export AZURE_STORAGE_KEY=$(echo $KEYS | jq -r .[0].value)
        foo@bar:~$ az storage container create --name $PULIMI_STATE_CONTAINER --fail-on-exist 
        foo@bar:~$ pulumi login azblob://$PULIMI_STATE_CONTAINER
        ```

    - Using a local folder
        ```console
        foo@bar:~$ pulumi login file://~
        ```

1. Initialize the database schema
    - Create a databsae and SQL Server user for the integration
        ```sql
          CREATE USER fpintegration WITH PASSWORD = '<password>'
          CREATE DATABASE fp_data
        ```
    
    - Change ownership of the database to the integration user
        ```sql
          ALTER AUTHORIZATION ON fp_data TO fpintegration
        ```
    - Execute `sql/init_db.sql` in the query tool of your choice

## Deploying The Webhook Integration

1. If you have not already done so, log-in to Azure:
    ```console
    foo@bar:~$ az login
    ```

1.  Create a new stack (you may use any name you like, however, `dev`, `test`, and `prod` are best-practice examples):

    ```console
    foo@bar:~$ pulumi stack init dev
    ```

1.  Install NPM dependencies:

    ```console
    foo@bar:~$ npm install
    ```

1.  Configure Azure location:

    ```console
    foo@bar:~$ pulumi config set azure:location <location>
    ```

1.  Configure Freedom Platform tenant:

    ```console
    foo@bar:~$ pulumi config set freedomTenant <tenant>
    ```

1.  Configure backend database:

    ```console
    foo@bar:~$ pulumi config set --path db.host <host>
    foo@bar:~$ pulumi config set --path db.port <port>
    foo@bar:~$ pulumi config set --path db.database <database>
    foo@bar:~$ pulumi config set --path db.user <user>
    foo@bar:~$ pulumi config set --secret databasePassword
    value: <password>
    ```

1.  Run `pulumi up` to preview and deploy changes:

    ``` console
    foo@bar:~$ pulumi up
    Previewing update (dev):
    ...

    Updating (dev):
    ...
    Resources:
       + 15 created
    Duration: 5m54s
    ```

1.  Note the new webhook callback URL (you will use this to configure the connector in the platform):

    ```console
    foo@bar:~$ pulumi stack output triggerUrl
    https://fpprodint-events51525cfc.centralus-1.eventgrid.azure.net/api/events?aeg-sas-key=yEY7sa89yiPq8xTVf%2BojGEfktBOZm2Ovvp2ZPHNASj4%3D
    ```

1.  Provide the callback URL (from the previous step) to the Freedom Platform [support team](mailto://support@freedom-platform.com), so that the integration connector can be configured to call your new endpoint
