import * as azure from '@pulumi/azure';
import * as pulumi from '@pulumi/pulumi';

import TelemetryEventHandlerFactory from './functions/telemetry-event-handler';

const envConfig = new pulumi.Config();

interface DbConfig {
  host: string,
  port: number,
  user: string,
  database: string,
  schema: string,
}

const dbConfig = envConfig.requireObject<DbConfig>('db');
const env = pulumi.getStack() || 'dev';
const freedomTenant = envConfig.require('freedomTenant');

const namePrefix = `fp-${env}-integrations`;
const shortNamePrefix = `fp${env}int`;

const commonTags = {
  environment: pulumi.getStack() === 'prod' ? 'Production' : 'Development',
  app: pulumi.getProject(),
  'freedom-platform.io:tenant': freedomTenant,
  'freedom-platform.io:connector': 'webhooks',
};

const resourceGroup = new azure.core.ResourceGroup(`${namePrefix}-webhooks-rg`, {
  tags: commonTags,
});

const current = azure.core.getClientConfig({});
const keyVault = new azure.keyvault.KeyVault(`${shortNamePrefix}-vault`, {
  softDeleteEnabled: true,
  location: resourceGroup.location,
  resourceGroupName: resourceGroup.name,
  tenantId: current.then(({ tenantId }) => tenantId),
  purgeProtectionEnabled: false,
  skuName: 'standard',
  tags: commonTags,
});

const dbPasswordSecret = new azure.keyvault.Secret(`${shortNamePrefix}-dbpass`, {
  keyVaultId: pulumi.interpolate`${keyVault.id}`,
  value: envConfig.requireSecret('databasePassword'),
  tags: commonTags,
});

const telemetryEventHttpFunction = new azure.appservice.HttpFunction('OnTelemetryEvent', {
  route: 'events/telemetry',
  methods: ['POST', 'OPTIONS'],
  callbackFactory: TelemetryEventHandlerFactory,
});

const appSettings = {
  FP_DB_HOST: dbConfig.host,
  FP_DB_PORT: dbConfig.port,
  FP_DB_USER: dbConfig.user,
  FP_DB_DATABASE: dbConfig.database,
  FP_DB_SCHEMA: dbConfig.schema,
  FP_DB_PASSWORD: pulumi.interpolate`@Microsoft.KeyVault(SecretUri=${keyVault.vaultUri}secrets/${dbPasswordSecret.name}/${dbPasswordSecret.version})`,
  APPINSIGHTS_INSTRUMENTATIONKEY: pulumi.output(''),
  APPLICATIONINSIGHTS_CONNECTION_STRING: pulumi.output(''),
};

const includeAnalytics = envConfig.require('includeAnalytics');
if (includeAnalytics) {
  const appInsights = new azure.appinsights.Insights(`${shortNamePrefix}-ai`, {
    location: resourceGroup.location,
    resourceGroupName: resourceGroup.name,
    applicationType: 'Node.JS',
  });

  appSettings.APPINSIGHTS_INSTRUMENTATIONKEY = appInsights.instrumentationKey;
  appSettings.APPLICATIONINSIGHTS_CONNECTION_STRING = pulumi.interpolate`InstrumentationKey=${appInsights.instrumentationKey}`;
}

const app = new azure.appservice.MultiCallbackFunctionApp(`${namePrefix}-app`, {
  resourceGroupName: resourceGroup.name,
  functions: [telemetryEventHttpFunction],
  identity: {
    type: 'SystemAssigned',
  },
  appSettings,
  tags: commonTags,
});

// eslint-disable-next-line no-new
new azure.keyvault.AccessPolicy(`${namePrefix}-vault-access-self`, {
  keyVaultId: pulumi.interpolate`${keyVault.id}`,
  tenantId: current.then(({ tenantId }) => tenantId),
  objectId: current.then(({ objectId }) => objectId),
  secretPermissions: [
    'backup',
    'delete',
    'get',
    'list',
    'purge',
    'recover',
    'restore',
    'set',
  ],
});

// eslint-disable-next-line no-new
new azure.keyvault.AccessPolicy(`${namePrefix}-vault-access-msi`, {
  keyVaultId: pulumi.interpolate`${keyVault.id}`,
  tenantId: current.then(({ tenantId }) => tenantId),
  objectId: app.functionApp.identity.apply(identity => identity.principalId || '11111111-1111-1111-1111-111111111111'), // Default is required to work around Pulumi quirk (Should return Unknown not undefined)
  keyPermissions: ['get', 'list'],
  secretPermissions: ['get', 'list'],
});

const eventGridDomain = new azure.eventgrid.Domain(`${shortNamePrefix}-events`, {
  location: resourceGroup.location,
  resourceGroupName: resourceGroup.name,
  inputSchema: 'CloudEventSchemaV1_0',
  tags: commonTags,
});

// eslint-disable-next-line no-new
new azure.eventgrid.EventSubscription(`${shortNamePrefix}-telem`, {
  scope: eventGridDomain.id,
  eventDeliverySchema: 'CloudEventSchemaV1_0',
  webhookEndpoint: {
    url: pulumi.interpolate`${app.endpoint}events/telemetry`,
  },
  includedEventTypes: [
    'io.freedomplatform.telemetry.v1.value',
  ],
  subjectFilter: {
    subjectBeginsWith: `urn:freedom:${freedomTenant}`,
  },
});

export const { endpoint, primaryAccessKey } = eventGridDomain;
export const key = eventGridDomain.primaryAccessKey;
export const triggerUrl = pulumi
  .all([eventGridDomain.endpoint, eventGridDomain.primaryAccessKey])
  .apply(([ep, key]) => `${ep}?aeg-sas-key=${encodeURIComponent(key)}`);
