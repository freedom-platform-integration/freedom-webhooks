import * as assert from 'assert';
import { HttpRequest, HttpResponse, Context } from '@pulumi/azure/appservice';

import FreedomDbStore from './store';
import configFactory from './config';

import { validateTelemetryEvent, validateCloudEvent } from './validators';

const validateEvent = event => {
  const { source, type } = event;
  const validCloudEvent = validateCloudEvent(event);
  if (!validCloudEvent) {
    throw new Error('CloudEvents schema validation error');
  }
  assert(source === 'io.freedomplatform.telemetry', 'invalid event source');
  assert(type === 'io.freedomplatform.telemetry.v1.value', 'invalid event type');
  const validTelemetryEvent = validateTelemetryEvent(event.data);
  if (!validTelemetryEvent) {
    throw new Error(`Telemetry schema validation error: ${validateTelemetryEvent.errors.reduce((acc, err) => `${acc}, [path: ${err.dataPath}, data: ${err.data}, message: ${err.message}]`, '')}`);
  }
};

interface Headers {
  [key: string]: string,
};

interface ComplexMetricValue {
  value: number | boolean,
};

type MetricValue = ComplexMetricValue | number | boolean;

const isComplexValue = (object): object is ComplexMetricValue => ('value' in object);

const TelemetryEventHandler = () => {
  const config = configFactory();
  const store = FreedomDbStore(config.db);

  const validateCloudEventEndpoint = async (ctx: Context<HttpResponse>, request: HttpRequest): Promise<HttpResponse> => {
    ctx.log('Validate request received');
    const headers: Headers = {};
    if (request.headers['webhook-request-origin']) {
      headers['Webhook-Allowed-Origin'] = request.headers['webhook-request-origin'];
    } else {
      headers['Webhook-Allowed-Origin'] = 'eventgrid.azure.net';
    }

    if (request.headers['webhook-request-rate']) {
      headers['Webhook-Allowed-Rate'] = request.headers['webhook-request-rate'];
    }

    return { status: 204, body: null, headers };
  };

  const handleTelemetryEvent = async (ctx: Context<HttpResponse>, event) => {
    try {
      validateEvent(event);

      const {
        data: {
          timestamp,
          context: {
            tenant,
          },
          element: {
            urn: elementUrn,
            name,
            label,
          },
          telemetry,
        },
      } = event;

      const upsertCount = await store.upsertElement({
        elementUrn,
        tenant,
        name,
        label,
        effectiveTime: (timestamp * 1000),
      });

      const insertCounts = await Promise.all(Object.entries<MetricValue>(telemetry)
        .map(([path, value]) => store.insertValue({
          elementUrn,
          path,
          timestamp: (timestamp * 1000),
          value: isComplexValue(value) ? value.value : value,
        })));

      const insertCount = [insertCounts.reduce((acc, count) => acc + count[0], 0)];
      ctx.log.info('posted', event, 'upserts', upsertCount, 'inserts', insertCount);
      return { status: 200, body: null };
    } catch (err) {
      ctx.log.error('error', err.message);
      throw err;
    }
  };

  return {
    validateCloudEventEndpoint,
    handleTelemetryEvent,
  };
};

const CallbackFactory = () => {
  const handler = TelemetryEventHandler();
  const callback = async (context: Context<HttpResponse>, request: HttpRequest) => {
    try {
      // CloudEvents endpoint validation (required)
      if (request.method === 'OPTIONS') {
        return handler.validateCloudEventEndpoint(context, request);
      }
      return handler.handleTelemetryEvent(context, request.body);
    } catch (err) {
      context.log.error(err);
      return { status: 500, body: err.message };
    }
  };

  return callback;
};

export default CallbackFactory;
