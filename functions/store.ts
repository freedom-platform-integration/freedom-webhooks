import * as sql from 'mssql';

const FreedomDbStore = ({
  host,
  port,
  user,
  password,
  database,
}) => {
  const pool = new sql.ConnectionPool({
    user,
    password,
    server: host,
    port,
    database,
    pool: {
      max: 10,
      min: 0,
      idleTimeoutMillis: 60000,
    },
    options: {
      encrypt: true,
    },
  });
  const poolConnect = pool.connect();

  pool.on('error', err => {
    console.error('connection pool error', err.message); // eslint-disable-line no-console
  });

  const upsertElement = async ({
    elementUrn,
    tenant,
    name,
    label,
    effectiveTime,
  }) => {
    try {
      await poolConnect;
      const result = await pool
        .request()
        .input('Urn', elementUrn)
        .input('Tenant', tenant)
        .input('Name', name)
        .input('Label', label)
        .input('EffectiveTime', sql.DateTime, new Date(effectiveTime))
        .execute('s_ElementDetails_Upsert');
      return result.rowsAffected;
    } catch (err) {
      if (err.name !== 'RequestError') {
        throw err;
      }
      return [0];
    }
  };

  const insertValue = async ({
    elementUrn,
    path,
    timestamp,
    value,
  }) => {
    try {
      await poolConnect;
      const result = await pool
        .request()
        .input('ElementUrn', elementUrn)
        .input('MetricPath', path)
        .input('CaptureTime', new Date(timestamp))
        .input('Value', value)
        .execute('s_MetricValue_Insert');
      return result.rowsAffected;
    } catch (err) {
      if (err.name !== 'RequestError') {
        throw err;
      }
      return [0];
    }
  };

  return {
    upsertElement,
    insertValue,
  };
};

export default FreedomDbStore;
