import * as assert from 'assert';

const config = () => {
  const res = {
    db: {
      host: process.env.FP_DB_HOST,
      port: Number.parseInt(process.env.FP_DB_PORT, 10) || 1433,
      user: process.env.FP_DB_USER,
      password: process.env.FP_DB_PASSWORD,
      database: process.env.FP_DB_DATABASE,
    },
  };
  assert(res.db.host, 'missing required environment variable: FP_DB_HOST');
  assert(res.db.user, 'missing required environment variable: FP_DB_USER');
  assert(res.db.password, 'missing required environment variable: FP_DB_PASSWORD');
  assert(res.db.database, 'missing required environment variable: FP_DB_DATABASE');

  return res;
};

export default config;
