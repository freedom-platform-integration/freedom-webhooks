import * as Ajv from 'ajv';

import * as cloudEventsSchema from './schemas/cloudevents.v1.json';
import * as telemetrySchema from './schemas/io.freedomplatform.telemetry.v1.json';

const ajv = new Ajv({ allErrors: true });

export const validateCloudEvent = ajv.compile(cloudEventsSchema);
export const validateTelemetryEvent = ajv.compile(telemetrySchema);
