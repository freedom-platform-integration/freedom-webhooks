DROP TABLE IF EXISTS dbo.MetricValues
DROP TABLE IF EXISTS dbo.Elements

CREATE TABLE dbo.Elements (
    ElementURN varchar(255) NOT NULL,
    Tenant varchar(32) NOT NULL,
    Name varchar(32) NOT NULL,
    Label varchar(255),
    LastUpdate datetime,
    CONSTRAINT PK_ElementURN PRIMARY KEY (ElementURN),
    CONSTRAINT UC_Element UNIQUE (Tenant, Name)
)
GO

DROP INDEX IF EXISTS idx_element_tenant_name ON dbo.Elements
CREATE INDEX idx_element_tenant_name
ON dbo.Elements (Tenant, Name)
GO

DROP PROCEDURE IF EXISTS s_ElementDetails_Upsert
GO

CREATE PROCEDURE s_ElementDetails_Upsert ( @Urn varchar(255), @Tenant varchar(32), @Name varchar(32), @Label varchar(255), @EffectiveTime datetime )
AS
  SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
  BEGIN TRAN

    IF EXISTS ( SELECT * FROM dbo.Elements WITH (UPDLOCK) WHERE ElementURN = @Urn )

      UPDATE dbo.Elements
         SET Tenant = @Tenant,
             Name = @Name,
             Label = @Label,
             LastUpdate = @EffectiveTime
       WHERE ElementURN = @Urn
         AND LastUpdate < @EffectiveTime
         AND (
           @Tenant <> Tenant OR @Name <> Name OR @Label <> Label
         );

    ELSE

      INSERT dbo.Elements ( ElementURN, Tenant, Name, Label, LastUpdate )
      VALUES ( @Urn, @Tenant, @Name, @Label, @EffectiveTime );

  COMMIT
GO

CREATE TABLE dbo.MetricValues (
    ElementURN varchar(255) NOT NULL,
    Path varchar(32) NOT NULL,
    CaptureTime datetime,
    Val numeric(18,6)
    CONSTRAINT PK_MetricTime PRIMARY KEY (ElementURN, Path, CaptureTime)
    CONSTRAINT FK_MetricElement FOREIGN KEY (ElementURN) REFERENCES dbo.Elements(ElementURN)
)
GO

DROP PROCEDURE IF EXISTS s_MetricValue_Insert
GO

CREATE PROCEDURE s_MetricValue_Insert ( @ElementUrn varchar(255), @MetricPath varchar(255), @CaptureTime datetime, @Value NUMERIC(18,6))
AS
  INSERT dbo.MetricValues ( ElementURN, Path, CaptureTime, Val )
  VALUES ( @ElementUrn, @MetricPath, @CaptureTime, @Value );
GO
