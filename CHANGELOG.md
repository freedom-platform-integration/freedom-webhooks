# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/freedom-platform-integration/freedom-webhooks/compare/v1.0.2...v1.1.0) (2020-08-11)


### Features

* add schema validation ([5a5e538](https://gitlab.com/freedom-platform-integration/freedom-webhooks/commit/5a5e5389b6749e4f5eb27d68d6a82092a2df6a8c))


### Bug Fixes

* correct resource labels ([2f24ce8](https://gitlab.com/freedom-platform-integration/freedom-webhooks/commit/2f24ce8d1324868b5a6614503320973622350b73))
* correct return values ([d084ca6](https://gitlab.com/freedom-platform-integration/freedom-webhooks/commit/d084ca647032b837aa158c84a2ec6666f7dcf8f9))

### [1.0.2](https://gitlab.com/freedom-platform-integration/freedom-webhooks/compare/v1.0.1...v1.0.2) (2020-08-10)

### [1.0.1](https://gitlab.com/freedom-platform-integration/freedom-webhooks/compare/v1.0.0...v1.0.1) (2020-08-07)


### Bug Fixes

* add missing node dependencie ([9573dfa](https://gitlab.com/freedom-platform-integration/freedom-webhooks/commit/9573dfa7e37ebc761fc51fd4289b1269cefbe4a4))
